#!/bin/bash
# This code is licensed under CC0 1.0 Universal: https://creativecommons.org/publicdomain/zero/1.0/legalcode
# Author: Andre Klapper <ak-47@gmx.net>
# Date: 2020-11-22
#
# Looking at this file you will quickly realize that I'm not a coder. ;)
#
# Note:
# Covers UI translations in the git master branches of projects under GNOME/ only.
# Ignores other groups and ignores docs!
# Not for "who's most active", but rather about teams that might only existing on paper

# How far to go back in Git commit statistics:
deadline="2016-01-01"

# Get a list of all language codes by parsing the HTTP links:
curl --silent -o allteams.web.tmp  https://l10n.gnome.org/languages/
grep " <a href=\"/teams/" allteams.web.tmp > allteams.web1.tmp
sed 's/\/">.*//' allteams.web1.tmp > allteams.web2.tmp
sed 's/.*teams\///' allteams.web2.tmp > allteams.web3.tmp
mapfile -t teams < allteams.web3.tmp

# Get a list of all team codes by parsing the RSS links:
grep "href=\"/rss/" allteams.web.tmp > alllangs.web1.tmp
sed 's/\/">.*//' alllangs.web1.tmp > alllangs.web2.tmp
sed 's/.*languages\///' alllangs.web2.tmp > alllangs.web3.tmp
mapfile -t langs < alllangs.web3.tmp

rm allteams.web.tmp allteams.web1.tmp allteams.web2.tmp
rm alllangs.web1.tmp alllangs.web2.tmp

synclangteamarrays=0
for langcode in "${langs[@]}"; do
  langcodeexistsatall=0
  countrepos=0
  latesttotalcommitdate=0
  declare -i counttotalcommits=0
  for repo in $( ls ); do
    if [ -d $repo ]; then
      cd $repo
      if [ -d "po" ]; then
        cd po
        if [ -f "$langcode.po" ]; then
          countcommits=$(git log --after=$deadline --format="%ci" "$langcode.po" | wc -l)
          # Get most recent commit date in this repo, but not made by myself:
          latestcommitdate=$(git log --perl-regexp --author='^((?!Andre Klapper).*)$' --format="%ci" -n1 "$langcode.po" | sed 's/\ .*//' | sed 's/-//g')
          if [ -n "$latestcommitdate" ]; then #not empty
            if [ "$latestcommitdate" -gt "$latesttotalcommitdate" ]; then
              latesttotalcommitdate=$latestcommitdate
            fi
          fi
          if ((countcommits > 0)); then
            let "countrepos+=1"
          fi
          counttotalcommits=$(($counttotalcommits+$countcommits))
          let "langcodeexistsatall+=1"
        fi
        cd ..
      fi
      cd ..
    fi
  done

  echo $langcode

  if [[ "$langcodeexistsatall" == 0 ]]; then
    echo "* /!\ $langcodeexistsatall translation files exist under GNOME/"
  else
    echo "* $langcodeexistsatall translation files exist under GNOME/"
  fi

  echo "* Latest language commit in Git master under GNOME/: $latesttotalcommitdate"

  echo "* $countrepos repos under GNOME/ updated in $counttotalcommits Git commits (since $deadline)"

  teamcode=${teams[$synclangteamarrays]}
  if [[ "$teamcode" == "$langcode" ]]; then
    echo "* Team code: $teamcode"
  else
    echo "* /!\ Team code: $teamcode"
  fi

  curl --silent https://l10n.gnome.org/teams/$teamcode/ --output $teamcode.web.tmp
  grep "         >" $teamcode.web.tmp | wc -l > $teamcode.othertranslators.tmp
  sed -i '1s/^/** Number of translators on l10n.gnome.org: /' $teamcode.othertranslators.tmp
  more $teamcode.othertranslators.tmp
  rm $teamcode.othertranslators.tmp

# Only enable if you want to print the name of the team coordinator:
#  grep "class=\"name main_feature\"" $teamcode.web.tmp > $teamcode.headname1.tmp
#  sed 's/<\/a>.*//' $teamcode.headname1.tmp > $teamcode.headname2.tmp
#  sed 's/.*\">//' $teamcode.headname2.tmp > $teamcode.headname3.tmp
#  sed 's/&nbsp;/ /g' $teamcode.headname3.tmp > $teamcode.headname4.tmp
#  sed -i '1s/^/** Team coordinator: /' $teamcode.headname4.tmp
#  more $teamcode.headname4.tmp
#  rm $teamcode.headname*.tmp

# Only enable if you want to print the obfuscated email address of the team coordinator:
#  grep "class=\"email\"" $teamcode.web.tmp > $teamcode.heademail1.tmp
#  sed 's/<\/a>.*//' $teamcode.heademail1.tmp > $teamcode.heademail2.tmp
#  sed 's/.*\">//' $teamcode.heademail2.tmp > $teamcode.heademail3.tmp
#  sed 's/&nbsp;/ /g' $teamcode.heademail3.tmp > $teamcode.heademail4.tmp
#  sed -i '1s/^/** Team coordinator email: /' $teamcode.heademail4.tmp
#  more $teamcode.heademail4.tmp
#  rm $teamcode.heademail*.tmp

  rm $teamcode.web.tmp

  sleep 5
  synclangteamarrays=$(($synclangteamarrays+1))
  countrepos=0
  countcommits=0
  counttotalcommits=0
  latestcommitdate=0
  latesttotalcommitdate=0
done
rm allteams.web3.tmp alllangs.web3.tmp
